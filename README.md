# fresco templates

fresco templates are used to bootstrap new fresco sites.  They are stored in buckets and used as the source bucket when copying to a target.

## Deploy

Every time you do a git push to master, the site will be deployed via CI/CD pipeline. This particular template gets pushed to an S3 bucket that lives at:

http://fresco-page-template-1.s3-website-us-east-1.amazonaws.com

